require 'rails_helper'

RSpec.describe User do
  let(:user) { User.new }

  describe "attributes" do
    it 'should have database attributes' do
      expect(user).to respond_to(:username)
      expect(user).to respond_to(:email)
      expect(user).to respond_to(:authn_token)
      expect(user).to respond_to(:encrypted_password)

      ## Recoverable
      expect(user).to respond_to(:reset_password_token)
      expect(user).to respond_to(:reset_password_sent_at)

      ## Rememberable
      expect(user).to respond_to(:remember_created_at)

      ## Trackable
      expect(user).to respond_to(:sign_in_count)
      expect(user).to respond_to(:current_sign_in_at)
      expect(user).to respond_to(:last_sign_in_at)
      expect(user).to respond_to(:current_sign_in_ip)
      expect(user).to respond_to(:last_sign_in_ip)

      ## Audit Stamps
      expect(user).to respond_to(:created_at)
      expect(user).to respond_to(:updated_at)
    end
  end

  describe "validations" do
    it { should validate_presence_of(:username) }
    it { should validate_presence_of(:email) }
    it { should validate_presence_of(:password) }
    it { should validate_presence_of(:role) }
    it { should validate_inclusion_of(:role).in_array(described_class::USER_ROLES) }

    context 'if new_record?' do
      before(:each) do
       allow(subject).to receive(:new_record?).and_return(true)
      end

      it { should validate_length_of(:password).is_at_least(8) }
      it { should validate_length_of(:password).is_at_most(128) }
    end
  end
end