require 'rails_helper'

RSpec.describe TvShowStatistic do
  let(:tv_show_statistic) { TvShowStatistic.new }

  describe "attributes" do
    it 'should have database attributes' do
      expect(tv_show_statistic).to respond_to(:category)
      expect(tv_show_statistic).to respond_to(:statistic_type)
      expect(tv_show_statistic).to respond_to(:data)
      expect(tv_show_statistic).to respond_to(:count)
    end
  end

  describe "validations" do
    it { should validate_presence_of(:category) }
    it { should validate_presence_of(:statistic_type) }
  end
end