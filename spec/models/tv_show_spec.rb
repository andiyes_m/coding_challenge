require 'rails_helper'

RSpec.describe TvShow do
  let(:tv_show) { TvShow.new }

  describe "attributes" do
    it 'should have database attributes' do
      expect(tv_show).to respond_to(:name)
      expect(tv_show).to respond_to(:url)
      expect(tv_show).to respond_to(:tv_show_type)
      expect(tv_show).to respond_to(:language)
      expect(tv_show).to respond_to(:status)
      expect(tv_show).to respond_to(:runtime)
      expect(tv_show).to respond_to(:code)
      expect(tv_show).to respond_to(:genre)
      expect(tv_show).to respond_to(:premiered)

      ## Audit Stamps
      expect(tv_show).to respond_to(:created_at)
      expect(tv_show).to respond_to(:updated_at)
    end
  end

  describe "validations" do
    it { should validate_presence_of(:code) }
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:url) }
    it { should validate_presence_of(:tv_show_type) }
    it { should validate_presence_of(:language) }
    it { should validate_presence_of(:status) }
  end
end