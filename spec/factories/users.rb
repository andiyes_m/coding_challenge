FactoryGirl.define do
  factory :user, class: User do
    username "user"
    password "test1234"
    role "USER"
    email "test@yahoo.com"
  end

  factory :admin_user, class: User do
    username "admin"
    password "test1234"
    role "ADMIN"
    email "test@yahoo.com"
  end
end
