require 'rails_helper'

RSpec.describe TvShowDataRetrieveService do
  before(:each) do
    VCR.configure do |config|
      config.cassette_library_dir = "fixtures/vcr_cassettes"
      config.hook_into :webmock
    end
  end

  describe "POST create" do
    it "TvShow record count eq response record count" do
      VCR.use_cassette("tvmaze") do
        response = Net::HTTP.get_response(URI('http://api.tvmaze.com/search/shows?q=girls'))
        data = JSON.parse(response.body)
        TvShowDataRetrieveService.new.retrieve
        expect(TvShow.all.count).to eq data.size
      end
    end

    it "TvShowStatistics record count not eq 0" do
      VCR.use_cassette("tvmaze") do
        response = Net::HTTP.get_response(URI('http://api.tvmaze.com/search/shows?q=girls'))
        data = JSON.parse(response.body)
        TvShowDataRetrieveService.new.retrieve
        expect(TvShowStatistic.all.count).not_to eq 0
      end
    end
  end
end