require 'rails_helper'

include Warden::Test::Helpers

RSpec.describe Api::SessionsController do

  before(:each) do
    @request.env["devise.mapping"] = ::Devise.mappings[:user]
    @user = ::FactoryGirl.create(:admin_user)
  end

  describe "POST create" do
    it "generate authentication token" do
      @user.update_attribute :authn_token, nil
      post :create, {
        username: @user.username,
        password: "test1234"
      }
      @user.reload
      expect(@user.authn_token).not_to eq nil
    end

    it "returns hash of token" do
      post :create, {
        username: @user.username,
        password: "test1234"
      }
      @user.reload
      expect(response.body.to_json.try(:[], 'token')).not_to eq nil
    end
  end
end
