require 'rails_helper'

RSpec.describe Api::TvShowsController do
  before(:each) do
    TvShowDataRetrieveService.new.retrieve
    @authn_token = SecureRandom.hex
    @user = User.create!(username: "admin", role: "ADMIN", email: "test@yahoo.com", authn_token: @authn_token, password: "test1234")
    payload = {username: @user.username, authn_token: @user.authn_token}
    @token = JWT.encode payload, nil, 'none'
  end

  describe "TV Show api Test" do
    it "get index" do
      get :index, {}, format: :json
      expect(response.body.size).not_to eq 0
    end

    it "get new" do
      get :new, {}, format: :json
      expect(response.body).to eq TvShow.new.to_json
    end

    it "post create" do
      params = {code: "AAA",
        name: "AAA",
        url: "aaa",
        tv_show_type: "AAA",
        language: "ID",
        status: "AAA",
        runtime: 0
      }
      post :create, {data: params}.merge({token: @token})
      expect(response.body).to eq TvShow.order(:created_at).last.to_json
    end

    it "get show" do
      @scope = TvShow.first
      get :show, id: @scope.id, format: :json
      expect(response.body).to eq @scope.to_json
    end

    it "get edit" do
      @scope = TvShow.first
      get :edit, id: @scope.id, format: :json
      expect(response.body).to eq @scope.to_json
    end

    it "put update" do
      @scope = TvShow.first
      put :update, {id: @scope.id, token: @token, data: {name: "AAA"}}
      @scope.reload
      expect(response.body).to eq @scope.to_json
    end

    it "detroy tv show" do
      @scope = TvShow.first
      delete :destroy, {id: @scope.id, token: @token}
      expect(TvShow.find_by(id: @scope.id)).to eq nil
    end
  end
end