require 'rails_helper'

RSpec.describe Api::TvShowStatisticsController do
  before(:each) do
    TvShowDataRetrieveService.new.retrieve
    @authn_token = SecureRandom.hex
    @user = User.create!(username: "admin", role: "ADMIN", email: "test@yahoo.com", authn_token: @authn_token, password: "test1234")
    payload = {username: @user.username, authn_token: @user.authn_token}
    @token = JWT.encode payload, nil, 'none'
  end

  it "get index" do
    get :index, {token: @token}, format: :json
    expect(response.body.size).not_to eq 0
  end

  it "get show" do
    @scope = TvShowStatistic.first
    get :show, {id: @scope.id, token: @token}, format: :json
    expect(response.body).to eq @scope.to_json
  end

  it "get edit" do
    @scope = TvShowStatistic.first
    get :edit, {id: @scope.id, token: @token}, format: :json
    expect(response.body).to eq @scope.to_json
  end
end