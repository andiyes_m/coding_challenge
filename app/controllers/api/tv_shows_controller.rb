module Api
  class TvShowsController < ApplicationController
    before_action :set_tv_show, only: [:show, :edit, :update, :destroy]
    before_action :authenticate_user_from_token!, only: [:create, :update, :destroy]

    def index
      @tv_shows = TvShow.all
      render json: @tv_shows
    end

    def show
      render json: @tv_show
    end

    def edit
      render json: @tv_show
    end

    def new
      @tv_show = TvShow.new
      render json: @tv_show
    end

    def create
      @tv_show = TvShow.new(tv_show_params)
      @tv_show.save!

      render json: @tv_show
    end

    def update
      @tv_show.update!(tv_show_params)
      render json: @tv_show
    end

    def destroy
      @tv_show.destroy!

      render text: {message: "Record sucessfully deleted"}.to_json
    end
    private
      def set_tv_show
        @tv_show = TvShow.find(params[:id])
      end

      def tv_show_params
        params.require(:data).permit(:code, :name, :url, :tv_show_type, :language, :status, :runtime)
      end

      def authenticate_user_from_token!
        token = params[:token]
        raise "Invalid Token" unless token.present?
        decoded_token = JWT.decode token, nil, false
        raise "Invalid Token" unless decoded_token.present?
        hsh = decoded_token[0]
        user = User.find_by(username: hsh['username'])
        raise "Invalid Token" unless user.present?
        raise "Invalid Token" unless user.authn_token == hsh['authn_token']
        raise "Not Authorize User" unless user.role == "ADMIN"
      end
  end
end