module Api
  class TvShowStatisticsController < ApplicationController
    before_action :set_tv_show_statistic, only: [:show, :edit]
    before_action :authenticate_user_from_token!

    def index
      @tv_show_statistics = TvShowStatistic.all
      render json: @tv_show_statistics
    end

    def show
      render json: @tv_show_statistic
    end

    def edit
      render json: @tv_show_statistic
    end
    private
      def set_tv_show_statistic
        @tv_show_statistic = TvShowStatistic.find(params[:id])
      end

      def authenticate_user_from_token!
        token = params[:token]
        raise "Invalid Token" unless token.present?
        decoded_token = JWT.decode token, nil, false
        raise "Invalid Token" unless decoded_token.present?
        hsh = decoded_token[0]
        user = User.find_by(username: hsh['username'])
        raise "Invalid Token" unless user.present?
        raise "Invalid Token" unless user.authn_token == hsh['authn_token']
      end
  end
end