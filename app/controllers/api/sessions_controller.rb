module Api
  class SessionsController < ::Devise::SessionsController
    skip_before_action :require_no_authentication, only: [:create]
    skip_before_action :verify_signed_out_user, only: [:destroy]

    def create
      resource = User.find_by(username: params[:username])
      raise "Invalid username" if resource.nil?

      password_valid = resource.valid_password?(params[:password])
      raise "Invalid password" unless password_valid

      resource.update_attribute :authn_token, SecureRandom.hex

      payload = {username: resource.username, authn_token: resource.authn_token}
      token = JWT.encode payload, nil, 'none'

      render text: {token: token}.to_json
    end
  end
end