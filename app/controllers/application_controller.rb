class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  rescue_from ::RuntimeError, with: :runtime_error

  def runtime_error(e)
    render text: {error_message: e.to_s}.to_json
  end
end
