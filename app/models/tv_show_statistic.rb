class TvShowStatistic < ActiveRecord::Base
  validates :category, presence: true
  validates :statistic_type, presence: true
end