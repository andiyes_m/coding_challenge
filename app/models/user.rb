class User < ActiveRecord::Base
  USER_ROLES = %w(ADMIN USER)

  validates :username, presence: true
  validates :email, presence: true
  validates :password, presence: true, length: { in: 8..128 }, if: :new_record?
  validates :role, presence: true, inclusion: { in: USER_ROLES }

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
