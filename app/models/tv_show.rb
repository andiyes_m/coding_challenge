class TvShow < ActiveRecord::Base
  validates :code, presence: true
  validates :name, presence: true
  validates :url, presence: true
  validates :tv_show_type, presence: true
  validates :language, presence: true
  validates :status, presence: true
end
