class TvShowDataRetrieveService
  attr_accessor :url

  def initialize
    @url = "http://api.tvmaze.com/search/shows?q=girls"
  end

  def retrieve
    tv_show_type_hsh = {}
    language_hsh = {}
    status_hsh = {}
    runtime_hsh = {}
    genre_hsh = {}

    response = RestClient.get @url
    json_response = JSON.parse(response.body)
    json_response.each do |tv_show_json|
      tv_show_json = tv_show_json['show']
      tv_show = TvShow.find_or_initialize_by(code: tv_show_json['id'])

      tv_show.update!(
        name: tv_show_json['name'],
        url: tv_show_json['url'],
        tv_show_type: tv_show_json['type'],
        language: tv_show_json['language'],
        status: tv_show_json['status'],
        runtime: tv_show_json['runtime'] || 0,
        genre: tv_show_json['genres'],
        premiered: tv_show_json['premiered']
      )

      tv_show_type_hsh[tv_show.tv_show_type] = check_and_merge_hash(tv_show_type_hsh[tv_show.tv_show_type], tv_show)
      language_hsh[tv_show.language] = check_and_merge_hash(language_hsh[tv_show.language], tv_show)
      status_hsh[tv_show.status] = check_and_merge_hash(status_hsh[tv_show.status], tv_show)
      runtime_hsh[tv_show.runtime] = check_and_merge_hash(runtime_hsh[tv_show.runtime], tv_show)
      (tv_show_json['genres'] || []).each do |genre|
        genre_hsh[genre] = check_and_merge_hash(genre_hsh[genre], tv_show)
      end
    end

    update_tv_show_statistic(tv_show_type_hsh, 'tv_show_type')
    update_tv_show_statistic(language_hsh, 'language')
    update_tv_show_statistic(status_hsh, 'status')
    update_tv_show_statistic(runtime_hsh, 'runtime')
    update_tv_show_statistic(genre_hsh, 'genre')
  end

  def check_and_merge_hash(hsh, tv_show)
    unless hsh.present?
      hsh = {
        data: [{id: tv_show.id, code: tv_show.code, name: tv_show.name}],
        count: 1
      }
    else
      hsh[:count] = hsh[:count] + 1
      hsh[:data] << {id: tv_show.id, code: tv_show.code, name: tv_show.name}
    end
    hsh
  end

  def update_tv_show_statistic(hsh, statistic_type)
    hsh.each do |key, val|
      tv_show_statistic = TvShowStatistic.find_or_initialize_by(statistic_type: statistic_type, category: key.to_s)
      tv_show_statistic.update!(count: val[:count], data: val[:data])
    end
  end
end