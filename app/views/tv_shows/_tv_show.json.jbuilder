json.extract! tv_show, :id, :id, :name, :url, :tv_show_type, :language, :status, :runtime, :created_at, :updated_at
json.url tv_show_url(tv_show, format: :json)
