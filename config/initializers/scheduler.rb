require 'rufus-scheduler'

scheduler = ::Rufus::Scheduler.singleton
scheduler.every '60s', overlap: false do
  TvShowDataRetrieveService.new.retrieve
end