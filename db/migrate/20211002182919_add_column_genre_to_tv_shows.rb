class AddColumnGenreToTvShows < ActiveRecord::Migration
  def change
    add_column :tv_shows, :genre, :jsonb, null: false, default: []
    add_column :tv_shows, :premiered, :date
  end
end
