class AddColumnCodeToTvShows < ActiveRecord::Migration
  def change
    add_column :tv_shows, :code, :string, null: false
  end
end
