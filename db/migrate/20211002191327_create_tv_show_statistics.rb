class CreateTvShowStatistics < ActiveRecord::Migration
  def change
    create_table :tv_show_statistics, id: :uuid do |t|
      t.string :category, null: false
      t.string :statistic_type, null: false
      t.jsonb :data
      t.integer :count, null: false, default: 0
    end
  end
end
