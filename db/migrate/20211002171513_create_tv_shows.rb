class CreateTvShows < ActiveRecord::Migration
  def change
    create_table :tv_shows, id: :uuid do |t|
      t.string :name, null: false
      t.string :url, null: false
      t.string :tv_show_type, null: false
      t.string :language, null: false
      t.string :status, null: false
      t.integer :runtime, null: false, default: 0

      t.timestamps null: false
    end
  end
end
